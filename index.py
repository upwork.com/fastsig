from bottle import Bottle
from bottle import request
from bottle import response
from bottle import redirect
from bottle import template


import pydocusign
import uuid


integration_key = '2c54a4ac-a308-4fdf-9830-22d669ea90d1'
sandbox_uri = 'https://demo.docusign.net/restapi/v2'
production_uri = 'https://account.docusign.com/oauth/auth?'
email_message_subject = 'Please DocuSign: OMMA - Adult Patient_Physician Recommendation Form.pdf'
email_message_body = 'This document needs your signature.'


client = pydocusign.DocuSignClient(
    root_url=sandbox_uri,
    username='iamjj@yandex.com',
    password='3rb4o2wl7D',
    integrator_key=integration_key,
)


def build_signers(signer_email, signer_full_name):
    signers = [
        pydocusign.Signer(
            email=signer_email,
            name=signer_full_name,
            recipientId=1,
            clientUserId=str(uuid.uuid4()),
            tabs=[
                pydocusign.SignHereTab(
                    documentId=1,
                    pageNumber=1,
                    xPosition=259,
                    yPosition=580,
                ),
                pydocusign.DateSignedTab(
                    documentId=1,
                    pageNumber=1,
                    xPosition=460,
                    yPosition=619
                )
            ],
            emailSubject=email_message_subject,
            emailBody=email_message_body,
            supportedLanguage='en',
        )
    ]
    return signers


def signing_url(signers):
    pdf = open('doc/omma.pdf', 'rb')

    envelope = pydocusign.Envelope(
        documents=[
            pydocusign.Document(
                name='document.pdf',
                documentId=1,
                data=pdf,
            )
        ],
        emailSubject='Please DocuSign: OMMA - Adult Patient_Physician Recommendation Form.pdf',
        emailBlurb='This is the body',
        status=pydocusign.Envelope.STATUS_SENT,
        recipients=signers,
    )

    client.create_envelope_from_documents(envelope)
    envelope.get_recipients()

    return envelope.post_recipient_view(
                envelope.recipients[0],
                returnUrl='http://10.64.247.216:8080/callback'
        )

base = Bottle()

@base.route('/signer')
def login():
    return template('index')


@base.route('/sign', method='POST')
def sign_document():
    email = request.POST.get('email')
    fullname = request.POST.get('fullname')

    signers = build_signers(email, fullname)
    url = signing_url(signers)

    # return f'<a href={url}>{url}</a></br>'
    return template('sign.html', url=url)
    # return url
@base.route('/callback')
def callback():
    return "Successfull"


base.run(host='0.0.0.0', port=8080, debug=True, reloader=True)